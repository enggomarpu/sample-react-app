import React, { useState, useEffect } from 'react';
// import { makeStyles } from '@material-ui/core/styles';
import {
 
  Typography,
  Grid,
  Button,
  Dialog,
  DialogContent,
  DialogActions,
  DialogTitle,
  makeStyles,
  FormControl,
  TextField,
  CircularProgress,
 
} from '@material-ui/core';
import { useMutation, useQuery } from '@apollo/client';
import { useToasts } from 'react-toast-notifications';
import { CREATE_USER_GROUP, GET_ALL_USERSGROUPS } from '../shared/constants';
import { globalStyles } from '../globalstyles';

const useStyles = makeStyles((theme) => ({
  mainContainer: {},
  formContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    padding: theme.spacing(2, 5, 5, 5)
  },
  textField: {
    '& .MuiTextField-root': {
      width: '100%',
      margin: 0
    },
    '& .MuiOutlinedInput-root': {
      backgroundColor: 'rgba(82, 107, 198, 0.32)'
    }
  },
  gridItem: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: '10px 0'
  },
  stepper: {
    '& .MuiStepLabel-labelContainer span': {
      fontSize: '24px'
    },
    padding: theme.spacing(5, 0, 5, 0)
  },
  dialogTitle: {
    backgroundColor: '#4b286d',
    color: '#fff'
  },
  modalFooter: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: theme.spacing(4),
    '& .MuiButton-root': {
      color: '#fff',
      boxShadow: 'none',
      textTransform: 'capitalize',
      padding: '10px 30px'
    },
    '& .MuiButton-label': {
      fontSize: '16px'
    }
    // '& .MuiButton-root.Mui-disabled': {
    //   backgroundColor: '#ffffff !important',
    // },
  },
  cancelButton: {
    '&.MuiButton-root': {
      backgroundColor: '#BDBDBD'
    }
  },
  nextButton: {
    '&.MuiButton-root': {
      backgroundColor: '#000'
    }
  },
  asterikClass: {
    color: 'red'
  },
  errorStyle: {
    // marginBottom: '10px'
  }
}));

const AddUserGroup = function (props) {

  const classes = useStyles();
  const globalClasses = globalStyles();
  const user = JSON.parse(localStorage.getItem('user'));
  const { addToast } = useToasts();
  const [userGrouptTxt, setUserGroupTxt] = useState('')
  const [userGrouptTxtError, setUserGroupTxtError] = useState('')

  const [addUserGroup, { loading, error, data }] = useMutation(CREATE_USER_GROUP, {
    variables: {
      createUserGroupInput: {
      id: Date.now,
      GroupName: userGrouptTxt
      },
    },
    onError: () => {
      //Do nothing
    },
    onCompleted: () => {
      addToast('User Group Created Successfully', { appearance: 'success' })
      //history.push("/welcomepage")
      props.handleClose()
    },
    refetchQueries: [{ query: GET_ALL_USERSGROUPS }],
    awaitRefetchQueries: true,
  })
 

  const textHandler = (e) => {
    const { value } = e.target;
    setUserGroupTxt(value)
    setUserGroupTxtError('')
  };

  const saveHandler = async () => {
    let pErr = false 
      if(userGrouptTxt === ''){
        setUserGroupTxtError('Please enter usergroup value')
        pErr = true
      }
    if(!pErr){
      await addUserGroup()
    }  
  };

  return (
    <Dialog
      open={props.open}
      onClose={props.handleClose}
      maxWidth="sm"
      fullWidth
      // PaperComponent={PaperComponent}
      aria-labelledby="draggable-dialog-title">
      <DialogTitle className={classes.dialogTitle}>
        <Typography variant="h6">User Group Wizard</Typography>
      </DialogTitle>

      <DialogContent className={classes.mainContainer}>
        <Grid container className={classes.formContainer}>
          <Grid container xs={12} className={classes.gridItem}>
          <Grid item xs={4}>
              <Typography variant="h5">User Group Name</Typography>
            </Grid>

            <Grid item xs={8}>
              <FormControl fullWidth className={classes.textField}>
                <TextField
                  name="GroupName"
                  variant="outlined"
                  placeholder="Group Name"
                  onChange={textHandler}
                  // size="small"
                 
                  inputProps={{
                    maxLength: 500
                  }}
                />
              </FormControl>
              <Typography
                className={classes.errorStyle}
                component="p"
                color="error"
                variant="body2"
              >
               {userGrouptTxtError}
              </Typography>
            </Grid>
          </Grid>
        </Grid>


        {(loading) && (
          <div className={globalClasses.loadingIndicator}>
            <CircularProgress />
          </div>
        )}

      </DialogContent>

      <DialogActions className={classes.modalFooter}>
        <Button autoFocus onClick={props.handleClose} className={classes.cancelButton}>
          Cancel
        </Button>
        <Button className={classes.nextButton} onClick={saveHandler}>
          Submit
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default AddUserGroup;

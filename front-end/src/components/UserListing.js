import React, { useState, useEffect } from 'react';
import {
  Button,
  Container,
  Grid,
  IconButton,
  makeStyles,
  Chip,
  Typography,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  CircularProgress,
  TablePagination,
  TableSortLabel
} from '@material-ui/core';
import SortIcon from '@material-ui/icons/Sort';
import FilterIcon from '@material-ui/icons/Filter';
import EditIcon from '@material-ui/icons/Edit';
import { useQuery } from '@apollo/client';
import moment from 'moment';
import PeopleIcon from '@material-ui/icons/PeopleAlt';
import AddUser from './AddUser';
import { globalStyles } from '../globalstyles';
import { GET_ALL_USERS, GET_USER_ID } from '../shared/constants';
import AddUserGroup from './AddUserGroup';
import { TextField } from '@mui/material';


const useStyles = makeStyles((theme) => ({
  tableContainer: {},
  tableHeaderBottomBorder: {
    '& .MuiTableCell-root': {
      borderBottom: '1px solid black'
    },
    '& .MuiTableCell-head': {
      color: 'black',
      fontSize: '16px',
      paddingTop: 7.5,
      paddingBottom: 7.5
    }
  },
  tableBodyBottomBorder: {
    '& .MuiTableCell-root': {
      borderBottom: 'none'
    },
    '& .MuiTableCell-body': {
      color: 'black',
      fontSize: '16px',
      paddingTop: 0,
      paddingBottom: 0
    },
    margin: 0
  },
  tableFirstColumnText: {
    '&:first-child': {
      color: '#56CCF2'
    },
    width: '25%'
  },
  chipStyle: {
    '& .MuiChip-label': {
      paddingLeft: '30px',
      paddingRight: '30px',
      fontWeight: 'bold'
    }
  },
  userGroupRow: {
    display: 'inline-block',
    paddingLeft: '20px',
    paddingRight: '20px'
  },
  userGroupText: {
    display: 'inline-block',
    paddingLeft: '20px',
    paddingRight: '20px'
  },
  headingWithIcon: {
    display: 'flex',
    alignItems: 'center',
    paddingTop: '10px',
    paddingBottom: '10px'
  }
}));

const UserListing = function (props) {
  const { loading, data, error } = useQuery(GET_ALL_USERS, {
    skip: false,
    onError: {
      // Do nothiing
    }
  });

  const classes = useStyles();
  const globalClasses = globalStyles();
  const [openCustomerSignupDialog, setOpenCustomerSignupDialog] = useState(false);
  const [allUsers, setAllUsers] = useState([]);
  const [userId, setUserId] = useState('');
  const [openEditModel, setOpenEditModel] = useState(false);
  const [formType, setFormType] = useState(1);
  const [openUserGroupDialog, setOpenUserGroupDialog] = useState(false)

  const [order, setOrder] = useState('asc');
  const [orderBy, setOrderBy] = useState('Email');
  const user = JSON.parse(localStorage.getItem('user'))
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [filterFn, setFilterFn] = useState({ fn: items => { return items; } })

  const columns = [
    { label: "User Email", field: "Email", align: "left" },
    { label: "Firstname", field: "Firstname", align: "left" },
    { label: "Lastname", field: "Lastname", align: "left" },
    { label: "Access Level", field: "roles", align: "left" },
    { label: "Last Updated", field: "UpdatedDate", align: "left" },
  ]


  useEffect(() => {
    if (data) {
      setAllUsers(data.getAllUserss);
    }
  }, [data]);

  const handleClick = () => {
    setOpenCustomerSignupDialog(true);
    setFormType(1);
  };
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  
  const handleRequestSort = (property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const descendingComparator = (a, b, orderBy) => {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  }

  const getComparator = (order, orderBy) => {
    return order === 'desc'
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
  }

  const stableSort = (array, comparator) => {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
  }
  const handleSearch = e => {
    let target = e.target;
    setFilterFn({
        fn: allUsers => {
            if (target.value === "")
                return allUsers;
            else
                return allUsers.filter(x => x.Email.toLowerCase().includes(target.value.toLowerCase()))
        }
    })
  }
  return (
    <>
      <div className={globalClasses.container}>
        <Grid container>
          <Grid item sm={8}>
            <header className={globalClasses.header}>
              <Typography variant="h6">User Details</Typography>
              <div className={globalClasses.headerButtons}>
              <TextField variant="outlined" onChange={handleSearch} />

                {/* <IconButton className={globalClasses.iconButtonStyle}>
                  <SortIcon style={{ margin: '0 5px' }} /> Sort
                </IconButton>
                <IconButton className={globalClasses.iconButtonStyle}>
                  <FilterIcon style={{ margin: '0 5px' }} /> Filter
                </IconButton> */}

               {user.roles === 2 && <Button
                  variant="contained"
                  onClick={() => {
                    setOpenCustomerSignupDialog(true);
                    setFormType(1);
                  }}
                >
                  Add User
                </Button>}
              </div>
            </header>
            {/* <TableHeader btnText="Add User" titleText="User Details" onButtonClick={handleClick} /> */}
            <TableContainer className={`${classes.tableContainer} table`}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow className={classes.tableHeaderBottomBorder}>
                    {columns.map((item) => {
                      return (
                        <TableCell
                          key={item.field}
                          sortDirection={orderBy === item.field ? order : false}
                        >
                          <TableSortLabel
                            active={orderBy === item.field}
                            direction={orderBy === item.field ? order : 'asc'}
                            onClick={() => handleRequestSort(item.field)}
                          >
                            {item.label}
                          </TableSortLabel>
                        </TableCell>
                      )
                    })}

                    {(user.roles === 2 || user.roles === 1) && <TableCell align="left">Edit User</TableCell>}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {loading ? ( <div className={globalClasses.loadingIndicator}>
                        <CircularProgress />
                      </div>
                    ) : (
                      stableSort(filterFn.fn(allUsers), getComparator(order, orderBy)).slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((cuser) => (
                        <TableRow
                          className={classes.tableBodyBottomBorder}
                          key={cuser.id}
                          sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                          <TableCell align="left">{cuser.Email}</TableCell>
                          <TableCell align="left">{cuser.Firstname}</TableCell>
                          <TableCell align="left">{cuser.Lastname}</TableCell>
                          <TableCell align="left">{cuser.roles === 3 ? 'User' : cuser.roles === 2 ? 'Admin' : 'Contributor'}</TableCell>
                          <TableCell align="left">{cuser.UpdatedDate !== null ? moment(cuser.UpdatedDate).fromNow() : moment(cuser.CreatedDate).fromNow()}</TableCell>
                          {(user.roles === 2 || user.roles === 1) &&
                          <TableCell align="left">
                            <IconButton
                              className={classes.iconEditButtonClasses}
                              onClick={() => {
                                setOpenCustomerSignupDialog(true);
                                setUserId(cuser.id);
                                setFormType(2);
                              }}
                            >
                              <EditIcon />
                            </IconButton>
                          </TableCell>}
                        </TableRow>
                      ))
                    )
                    // )
                    // : (
                    //   <div>No data from server</div>
                    // )
                  }
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25]}
              component="div"
              className={globalClasses.paginationIcons}
              count={allUsers.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </Grid>
          <Grid item sm={4} style={{ padding: '7px 20px' }}>
            <header className={globalClasses.header}>
              <Typography variant="h6">User Groups</Typography>
              <div className={globalClasses.headerButtons}>
                {user.roles === 2 && <Button variant="contained" onClick={() => {setOpenUserGroupDialog(true)} }>Add User Group</Button>}
              </div>
            </header>

            <Typography variant="h6" className={classes.headingWithIcon}>
              <PeopleIcon fontSize="small" />
              <span style={{ paddingLeft: '10px' }}>Tech Strategy Team </span>
            </Typography>
            <div>
              {/* <p ><span style={{padding: '0 20px'}}>Total Users<span></p>  */}
              <p className={classes.userGroupRow}>
                <span className={classes.userGroupText}>Total Users</span>
                <Chip label="1" className={classes.chipStyle} variant="outlined" />
              </p>
              <p className={classes.userGroupRow}>
                <span className={classes.userGroupText}>Active Users</span>
                <Chip label="1" className={classes.chipStyle} variant="outlined" />
              </p>
            </div>
            <Typography variant="h6" className={classes.headingWithIcon}>
              <PeopleIcon fontSize="small" />
              <span style={{ paddingLeft: '10px' }}> Business Team </span>
            </Typography>
            <div>
              <p className={classes.userGroupRow}>
                <span className={classes.userGroupText}>Total Users</span>
                <Chip label="1" className={classes.chipStyle} variant="outlined" />
              </p>
              <p className={classes.userGroupRow}>
                <span className={classes.userGroupText}>Active Users</span>
                <Chip label="1" className={classes.chipStyle} variant="outlined" />
              </p>
            </div>
          </Grid>
        </Grid>
      </div>
      {openCustomerSignupDialog && (
        <AddUser
          open={openCustomerSignupDialog}
          handleClose={() => setOpenCustomerSignupDialog(false)}
          userId={userId}
          formType={formType}
        />
      )}
        {openUserGroupDialog && (
        <AddUserGroup
          open={openUserGroupDialog}
          handleClose={() => setOpenUserGroupDialog(false)}
          //userId={userId}
          //formType={formType}
        />
      )}
    </>
  );
};
export default UserListing;

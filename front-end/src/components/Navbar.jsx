import {
  //alpha,
  AppBar,
  Avatar,
  Badge,
  Card,
  CardHeader,
  IconButton,
  Input,
  InputAdornment,
  InputBase,
  ListItem,
  ListItemText,
  makeStyles,
  Toolbar,
  Typography,
} from '@material-ui/core'
import { Cancel, Height, Mail, Notifications, Search } from '@material-ui/icons'
import { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { useGlobalContext } from '../context/globalContext'
import MenuOpenIcon from '@material-ui/icons/FormatAlignJustify'
import { Link } from 'react-router-dom'
import SearchIcon from '@material-ui/icons/Search'

const useStyles = makeStyles((theme) => ({
  toolbar: {
    display: 'flex',
    justifyContent: 'space-between',
    backgroundColor: '#4b286d',
  },
  logoLg: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  logoSm: {
    display: 'block',
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  search: {
    display: 'flex',
    alignItems: 'center',
    //backgroundColor: alpha(theme.palette.common.white, 0.15),
    '&:hover': {
      //backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    borderRadius: theme.shape.borderRadius,
    width: '50%',
    [theme.breakpoints.down('sm')]: {
      display: (props) => (props.open ? 'flex' : 'none'),
      width: '70%',
    },
    paddingLeft: '50px',
  },
  input: {
    color: 'white',
    marginLeft: theme.spacing(1),
  },
  cancel: {
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  searchButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  icons: {
    alignItems: 'center',
    display: (props) => (props.open ? 'none' : 'flex'),
  },
  badge: {
    marginRight: theme.spacing(2),
  },
  listItemStyle: {
    '&.MuiListItem-root': {
      textAlign: 'right',
    },
    '& .MuiTypography-body2': {
      fontSize: '1.1rem',
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
      fontWeight: 600,
      lineHeight: 1.43,
      letterSpacing: '0.1rem',
      marginRight: '15px',
      color: 'white',
    },
  },
  avatarStyle: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
  logoImage: {
    flexGrow: 1,
    whiteSpace: 'nowrap',
    //overflow: "hidden",
    //textOverflow: "ellipsis",
    fontSize: '1.8rem',
    lineHeight: '2.4rem',
    fontWeight: 500,
    padding: '0',
    display: 'flex',
    alignItems: 'center',
    '& img': {
      height: '50px',
      width: 'auto',
    },
  },
  textField: {
    //overflow: 'hidden',

    '& .MuiInputBase-input': {
      padding: '0 30px;',
      fontSize: '20px',
      fontWeight: 'bold',
      color: '#1c348a',
    },
    '& .MuiTypography-colorTextSecondary': {
      color: '#000',
      padding: '0px 40px',
    },
    '&.MuiInput-underline:before': {
      borderBottom: 0,
    },
    '&.MuiInput-underline:hover:not(.Mui-disabled):before': {
      borderBottom: 0,
    },
    '&.MuiInput-underline:after': {
      borderBottom: 0,
    },
    background: '#fff',
    borderRadius: 25,
    width: '75%',
    borderBottom: '0px',
    margin: '5px 0px',
    height: 48,
  },
}))

const Navbar = () => {
  const [open, setOpen] = useState(false)
  const classes = useStyles({ open })
  const user = JSON.parse(localStorage.getItem('user'))
  const history = useHistory()
  const { setLogout, toggleSideBar } = useGlobalContext()
  console.log('navbar', user)

  const logout = () => {
    setLogout()
    localStorage.clear()
    history.push('/')
  }
  return (
    <AppBar position='fixed'>
      <Toolbar className={classes.toolbar}>
        <IconButton className='white-color' onClick={toggleSideBar}>
          <MenuOpenIcon fontSize='large' />
        </IconButton>
        <div className={classes.logoImage}>
          <img
            src='https://apex.oracle.com/pls/apex/markitech/r/92807/files/static/v10/TELUS%20Logo%20White.png'
            alt='Telus-Logo'
          />
          <span>MSMB Stream Managment Tool</span>
        </div>
        {/* <Typography variant="h6" className={classes.logoLg}></Typography>
          <Typography variant="h6" className={classes.logoSm}>
          LAMAfffffffffffffffffffffffffffffffff
        </Typography> */}

        <div className={`${classes.search} searchbar`}>
          {/* <Search />
          <InputBase
            placeholder="Search..."
            className={classes.input}
            style={{ backgroundColor: "white" }}
          />
          <Cancel className={classes.cancel} onClick={() => setOpen(false)} /> */}
          <Input
            startAdornment={
              <InputAdornment
                position='end'
                className={classes.iconVisibilityButton}
              >
                <IconButton>
                  <SearchIcon />
                </IconButton>
              </InputAdornment>
            }
            className={classes.textField}
            //type={showNewPassword ? "text" : "password"}
            //onChange={textHandler}
            //value={newPassword}
            type='text'
            name='newpassword'
            placeholder='Search Keyword'
          />
        </div>

        <div className={classes.icons}>
          <Search
            className={classes.searchButton}
            onClick={() => setOpen(true)}
          />

          <ListItem className={classes.listItemStyle}>
            <ListItemText
              primary={
                <Typography variant='body2'>
                  {user && user.Firstname}
                </Typography>
              }
              secondary={
                <span style={{ cursor: 'pointer' }} onClick={logout}>
                  Logout
                </span>
              }
            />
            <Link to='/updateprofile'>
              <Avatar
                className={classes.avatarStyle}
                alt='Remy Sharp'
                src='data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxIQEhAQDxASEA8QEhAQDxAPDw8VEBAQFREWFxUSFhYYHSggGBolGxUVITEhJSkrLy4uFx8zODMtNyg5LysBCgoKDQ0NDg8NGisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAOEA4QMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAABAUBAwYCB//EADgQAAIBAQUECAQFBQEBAAAAAAABAgMEBREhMRJBUXEGMlJhgZGhsSJywdEzQmKi8BMjssLhkoL/xAAUAQEAAAAAAAAAAAAAAAAAAAAA/8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAwDAQACEQMRAD8A+zgAAAAAAAAAAAAAAAAGu0VlTjKcnhGKbYGwFbZ79s88lU2XwmnH1eXqWMXjms09GtAMgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAN4ZvQobx6SwhjGiv6ku0+ouXaK7pJezqSlRg8KcXhLD88lr4IowOnum9U1Ovaa2aezTprdkm2oLXVLHnmVl9XzK0fDFONJPFR3yfGX2KsACTYrwqUXjTm0t8XnF80RgB3Fz3xG0LDq1EsXDiuMeKLM+b0K0oSjODwlF4pn0Gw2lVacKi/MscOD0a88QN4AAAAAAAAAAAAAAAAAAAAAAAAAAFDfPSBU26dHCU1lKX5YPguLNnSW83SgqcHhUqJ5rWMN75vTzONAyAAABjEDIMYmVnpnyAEqxXhVov+3Npb4vOL8CZdt2SzqVFsqKcoxeraWTa3IqEB3Fz3zG0fC1sVUs445SXGP2LQ+bU6ji1KLwlF4prVM726bcq9OM9JdWa4SWvhv8AECYAAAAAAAAAAAAAAAAAAAAAAGu0T2YTl2YyfkmwOEvi0/1a1SW7acY/LHJffxIZhGUgPVGlKbUYpuT0SL2yXFFZ1XtPsxeEV46smXXYVRjn15dd/wCq7iaBHp2GlHSnDxim/Nm1U4rSKXJI9gDzsrgvIykZAHmccU1xTXocSdwcheVHYqzjuxbXJ5r3AjF/0QtOFSdN6TjtL5o/8b8igJ9wz2bRRfGWz/6TX1A7wAAAAAAAAAAAAAAAAAAAAANNtWNOouNOa/azcGscuOQHzMn3LR26scdI4zfhp6tEOrT2XKL1i3F+DwLnozDOpLujFerfsgL0AAAAAAAAp+kNk2kqqWccpfLufg/cuDDWOTzT1TA4gm3IsbRR+dPyz+hi9bIqVRxXVa2o9ye70JPRintWiD7KnL9rXu0B2wAAAAAAAAAAAAAAAAAAAAAG8Mwa7S/hf83gcPfsMK9RpYKT214rP1xLLo2v7c3xnh5RX3NHSWlnTnxTi/DNe7JPRz8J/O/ZAWoAAAAAAAAAA57pL16fyv3JPQ+K26kn2VBPm8X7IjdJOvD5P9mWPR6ls04vfKTl4Y4L2A6MAAAAAAAAAAAAAAAAAAAAANddfC+RsDQHO33R2qMuMcJLw19GzxcEMKSfalJrlp9CyqQ1i81mmuKPMYpJJLBLJJaJAegAAAAAAAAABRdIKLlOlh+ZbC57X/S8s1JR2ILRbMVyWRiUU8MUng8VitHxRIskcZY8MwJoAAAAAAAAAAAAAAAAAAAAAAANFoobWa13kSUcG1wLIg2qOEueYGoAAAAAAAAAAeqdNyyXqTqNPZWG/eabFHJvwJIAAAAAAAAAAAAAAAAAAAAAAAAA02uGKx3r2NwArAe68Nl4eK5HgAAAAAABA32OCeL4PDxAlUoYJL+YnoAAAAAAAAAAAAAAAAAAAAAAAAAAAUN63xrCi8tJTW/uj9wJ1ue0/ha2o5a6PgyPSqp5aPemRbn6j+Z+yJNehtZrKXuBuBBVolHJ589TYrZ+n1AlAiO2cI+bNNStKWry4ICRXtOGUc3x3ImXNNbMlj8W1i1jng0synI9ao4zUotxklk1zA7AFbdV6qr8M8I1PSXLv7iyAAAAAAAAAAAAAAAAAAAAAabRa4U+vNR7sc/LUDcCnr3/AAXUg5d8sl9yvrX1Wlo1BfpWfm8QJd+XlrSpvuqSX+K+pRGTAFxc/UfzP2RPKGx2p03xi9V9V3lzQrxmsYvHit65oD1UpqWq+5FnZWtM/cmgCslBrVNeB5LUxh3AVaIttWEljll9WWtqtkaeWsuyt3PgUtWo5Nyk8WwPMXhmsms01qmdRdF4/wBWOEvxI6/qXaRyx7o1ZQalF4SWjQHbg52hf8114xmuK+F/YsbPfVKWrcH+tZeayAsQYhJNYpprinijIAAAAAAAAAA0Wy2QpLam+SXWlyA3lfbL4p08k9uXCOniykt96Tq4rqw7KevN7yABYWq+Ks8k9iPCGvnqQGzAAAAAAAB6hNxeMXg1vR5AFvY7xUsIzye6W58+BYHMFzddp2lsvWOnfECbJ4ZvJLVlTa7ybxVPJdre+XAXracXsLRdbvfArwMmAAAAAAADZRryg8YScX3P+YlrZb+ksqkVJdqOUvLR+hTADsrLbadXqSxe+LykvAkHDJ4ZrJrRrVFvYL8lHCNX449r8y+4HRA8Ua0ZpSg1KL3o9gAABot1qVKDm+SXGW5HI2mvKpJym8W/JLgu4tek1X4oQ3KLl4t4fQpQAAAAAAAAAAAAAAb7FV2Jp7s0+WBoAGZPHFvV5vmYAAAAAAAAAAAAAAAJNhtsqMsY6PrRekl9+86yzWiNSKnHR+ae9M4ouOjddqcqe6S2l8y/57AdEAAOZ6Rfi/8AxH3ZVlv0kj/ci+MF6SZUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACdcrwrU+bX7WQSdcqxrU+bf7WB1gAAoOk+tPlL3RSAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACxuD8aPKf+LAA6kAAf/Z'
              />
            </Link>
          </ListItem>
        </div>
      </Toolbar>
    </AppBar>
  )
}

export default Navbar

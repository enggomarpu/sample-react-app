import React, { useEffect, useState } from 'react'
// import { makeStyles } from '@material-ui/core/styles';
import moment from 'moment'
import { globalStyles } from '../globalstyles'
import HighlightOffIcon from '@material-ui/icons/HighlightOff'
import { GET_LOGS_BY_ID } from '../shared/constants'

import { useToasts } from 'react-toast-notifications'
import {
  Button,
  TableBody,
  Dialog,
  DialogActions,
  DialogTitle,
  TableCell,
  TableContainer,
  TableHead,
  makeStyles,
  Table,
  TableRow,
  CircularProgress,
  DialogContent,
  Typography,
  IconButton,
} from '@material-ui/core'
import { useQuery } from '@apollo/client'

const useStyles = makeStyles((theme) => ({
  mainContainer: {},
  tableContainer: {},
  tableHeaderBottomBorder: {
    '& .MuiTableCell-root': {
      //borderBottom: "1px solid black",
    },
    '& .MuiTableCell-head': {
      color: 'black',
      fontSize: '16px',
      fontWeight: 600,
      paddingTop: 7.5,
      paddingBottom: 7.5,
    },
  },
  tableBodyBottomBorder: {
    '& .MuiTableCell-root': {
      borderBottom: 'none',
    },
    '& .MuiTableCell-body': {
      color: 'black',
      fontSize: '16px',
      paddingTop: 10,
      paddingBottom: 10,
    },
    margin: 0,
  },

  dialogTitle: {
    // '& .MuiDialogTitle-root':{
    //   flex: '1 1 auto'
    // },
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#4b286d',
    color: '#fff',
    '& .MuiIconButton-root': {
      //backgroundColor: "white",
      color: 'white',
      padding: 0,
    },
    '& .MuiSvgIcon-root': {
      fontSize: 40,
    },
  },
  modalFooter: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: theme.spacing(4),
    '& .MuiButton-root': {
      color: '#fff',
      boxShadow: 'none',
      textTransform: 'capitalize',
      padding: '10px 30px',
    },
    '& .MuiButton-label': {
      fontSize: '16px',
    },
    // '& .MuiButton-root.Mui-disabled': {
    //   backgroundColor: '#ffffff !important',
    // },
  },
  cancelButton: {
    '&.MuiButton-root': {
      backgroundColor: '#BDBDBD',
    },
  },
  nextButton: {
    '&.MuiButton-root': {
      backgroundColor: '#000',
    },
  },
  asterikClass: {
    color: 'red',
  },
  errorStyle: {
    //marginBottom: '10px'
  },
}))

const CustomerLogModel = (props) => {
  const classes = useStyles()
  const globalClasses = globalStyles()
  const { addToast } = useToasts()
  const [customerLogData, setCustomerLogData] = useState([])
  const user = JSON.parse(localStorage.getItem('user'))

  const { loading: loadingCustLog, data: dataCustData, error: errorCustError} = useQuery( GET_LOGS_BY_ID, {
    variables: { id: props && props.customerId },
    onError: () => {
      props.handleClose()
    },
  })

  useEffect(() => {
    if(dataCustData){
      console.log('log data', dataCustData)
      setCustomerLogData(dataCustData.getAllLogs)
    }

  }, [dataCustData])

  return (
    <Dialog
      open={props.open}
      onClose={props.handleClose}
      maxWidth='lg'
      fullWidth
      //PaperComponent={PaperComponent}
      aria-labelledby='draggable-dialog-title'
    >
      <DialogTitle disableTypography className={classes.dialogTitle}>
        <Typography variant='h6'>Customer Audit</Typography>
        <IconButton
          disableRipple
          disableTouchRipple
          onClick={props.handleClose}
          fontSize='large'
        >
          <HighlightOffIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent className={classes.mainContainer}>
        <TableContainer className={`${classes.tableContainer} table`}>
          <Table sx={{ minWidth: 650 }} aria-label='simple table'>
            <TableHead>
              <TableRow className={classes.tableHeaderBottomBorder}>
                <TableCell align='left'>Customer Name</TableCell>
                <TableCell align='left'>Date</TableCell>
                <TableCell align='left'>Type</TableCell>
                <TableCell align='right'>User</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {false ? (
                <div className={globalClasses.loadingIndicator}>
                  <CircularProgress />
                </div>
              ) : customerLogData.length > 0 ? ( customerLogData.map((customer) => (
                  <TableRow
                    className={classes.tableBodyBottomBorder}
                    key={customer}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell align='left'>{props.customerName}</TableCell>
                    <TableCell align='left'>{moment(customer && customer.CreatedDate).format('MM/DD/YYYY')}</TableCell>
                    <TableCell align='left'>{customer && customer.Message.split(' ')[0]}</TableCell>
                    <TableCell align='right'>{user.Email}</TableCell>
                  </TableRow>
                ))
              ) : (
                <div>No data from server</div>
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </DialogContent>

      <DialogActions className={classes.modalFooter}>
        <Button
          autoFocus
          onClick={props.handleClose}
          className={classes.cancelButton}
        >
          Cancel
        </Button>
        {/* <Button className={classes.nextButton} onClick={saveHandler}>Submit</Button> */}
      </DialogActions>
    </Dialog>
  )
}

export default CustomerLogModel

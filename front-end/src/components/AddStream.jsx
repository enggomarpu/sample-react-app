import React, { useState, useEffect } from 'react'
// import { makeStyles } from '@material-ui/core/styles';
import {
  Container,
  Typography,
  Grid,
  TextField,
  Button,
  Dialog,
  DialogContent,
  DialogActions,
  DialogTitle,
  makeStyles,
  FormControl,
  Select,
  MenuItem,
  Stepper,
  Step,
  StepLabel,
  IconButton,
  InputAdornment,
  FormGroup,
  FormControlLabel,
  Checkbox,
} from '@material-ui/core'
import { DatePicker } from '@material-ui/pickers'
import DateRangeIcon from '@material-ui/icons/DateRange'
import { useMutation, useQuery } from '@apollo/client'
import { GET_STREAM_BY_ID, UPDATE_STREAM } from '../shared/constants'
import { useToasts } from 'react-toast-notifications'


const useStyles = makeStyles((theme) => ({
  mainContainer: {},
  formContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    padding: theme.spacing(2, 5, 5, 5),
    // paddingLeft: 100,
    // paddingRight: 100,
    // '& .MuiTextField-root': {
    //   margin: theme.spacing(1),
    //   width: '300px',
    // },
    // '& .MuiButtonBase-root': {
    //   margin: theme.spacing(2),
    // },
  },
  textField: {
    '& .MuiTextField-root': {
      width: '100%',
      margin: 0,
    },
    '& .MuiOutlinedInput-root': {
      backgroundColor: 'rgba(82, 107, 198, 0.32)',
    },
    '& .MuiInputBase-input': {
      fontSize: '16px',
    },
  },
  gridItem: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: '10px 0',
  },
  stepper: {
    '& .MuiStepLabel-labelContainer span': {
      fontSize: '24px',
    },
    padding: theme.spacing(5, 0, 5, 0),
  },
  dialogTitle: {
    backgroundColor: '#4b286d',
    color: '#fff',
  },
  modalFooter: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: theme.spacing(2),
    '& .MuiButton-root': {
      color: '#fff',
      boxShadow: 'none',
      textTransform: 'capitalize',
      padding: '10px 30px',
    },
    '& .MuiButton-label': {
      fontSize: '16px',
    },
    // '& .MuiButton-root.Mui-disabled': {
    //   backgroundColor: '#ffffff !important',
    // },
  },
  cancelButton: {
    '&.MuiButton-root': {
      backgroundColor: '#BDBDBD',
    },
  },
  nextButton: {
    '&.MuiButton-root': {
      backgroundColor: '#000',
    },
  },
  checkboxContainer: {
    display: 'flex',
    //justifyContent: 'space-between'
  },
  formControl: {
    margin: theme.spacing(3),
  },
  checkboxStyle: {
    transform: 'scale(2)',
    marginRight: '20px',
  },
  checkboxLabel: {
    '& .MuiTypography-body1': {
      fontSize: '1.4rem',
    },
  },
  asterikClass: {
    color: 'red',
  },
}))

const AddStream = (props) => {
  const classes = useStyles()
  const steps = ['Customer Information', 'Service Information']
  const { addToast } = useToasts()

  const [stateValues, setStateValues] = useState({
      
    ServiceId: '',
    RatingGroupId: '',
    StreamId: '',
    StreamName: '',
    ApplicationName: '',
    ChargeCategory: '',
    ChargeFrequency: '',
    TrafficClassificationCriteria: '',
    APNName: '',
    TrafficIdentifier: '',
    TestCriteria: '',
    Note: ''
})
const [stateErrorValues, setStateErrorValues] = useState({
    
    ServiceId: '',
    RatingGroupId: '',
    StreamId: '',
    StreamName: '',
    ApplicationName: '',
    ChargeCategory: '',
    ChargeFrequency: '',
    TrafficClassificationCriteria: '',
    APNName: '',
    TrafficIdentifier: '',
    TestCriteria: '',
    Note: ''
})

  const { loading: loadingStreamById, data: dataStreamById, error: errorStreamById, } = useQuery(GET_STREAM_BY_ID, { 
    variables: { id: props && props.streamId },
    skip: props.formType === 1,
    onError: () => {
      props.handleClose()
    },
  })

  const [updateStreamMutation, { loading: loadingUpdate, data: dataUpdate, error: errorUpdate },] = useMutation(UPDATE_STREAM, {
    onCompleted: () => {
      addToast("User saved Successfully", { appearance: "success" });
      props.handleClose();
    },
    variables: {
      updateStreamInput: {
        ServiceId: stateValues.ServiceId,
        RatingGroupId: stateValues.RatingGroupId,
        StreamId: stateValues.StreamId,
        StreamName: stateValues.StreamName,
        ApplicationName: stateValues.ApplicationName,
        ChargeCategory: stateValues.ChargeCategory,
        ChargeFrequency: stateValues.ChargeFrequency,
        TrafficClassificationCriteria: stateValues.TrafficClassificationCriteria,
        APNName: stateValues.APNName,
        TrafficIdentifier: stateValues.TrafficIdentifier,
        TestCriteria: stateValues.TestCriteria,
        Note: stateValues.Note,
      },
      id: props && props.streamId,
    },
    //refetchQueries: [{ query: GET_ALL_USERS }],
    //refetchQueries: [{ query: GET_USER_ID, variables: { id: props && props.userId }  }],
    //awaitRefetchQueries: true,
    update: (cache) => {
      cache.evict({
        id: "ROOT_QUERY",
        field: "id",
      });
    },
    onError: () => {
      //Do nothing
    },
  });

  

  useEffect(() => {
    if (props.formType === 2) {
      if(dataStreamById) {
        console.log('hklh', dataStreamById)
        setStateValues((prevState) => ({...prevState, StreamId: dataStreamById.StreamById.StreamId}))
        setStateValues((prevState) => ({...prevState, StreamName: dataStreamById.StreamById.StreamName}))
        setStateValues((prevState) => ({...prevState, ApplicationName: dataStreamById.StreamById.ApplicationName}))
        setStateValues((prevState) => ({...prevState, ServiceId: dataStreamById.StreamById.ServiceId}))
        setStateValues((prevState) => ({...prevState, RatingGroupId: dataStreamById.StreamById.RatingGroupId}))

        setStateValues((prevState) => ({...prevState, ChargeCategory: dataStreamById.StreamById.ChargeCategory}))
        setStateValues((prevState) => ({...prevState, ChargeFrequency: dataStreamById.StreamById.ChargeFrequency}))

        setStateValues((prevState) => ({...prevState, TrafficClassificationCriteria: dataStreamById.StreamById.TrafficClassificationCriteria}))
        setStateValues((prevState) => ({...prevState, APNName: dataStreamById.StreamById.APNName}))

      }
    }

   
  }, [dataStreamById])

  const textHandler = (e) => {
    const { name, value } = e.target
    setStateValues({ ...stateValues, [name]: value })
    setStateErrorValues({ ...stateErrorValues, [name]: '' })
  }

  const saveHandler = async () => {
    await updateStreamMutation()
  }

  return (
    <Dialog
      open={props.open}
      onClose={props.handleClose}
      maxWidth='lg'
      fullWidth
      //PaperComponent={PaperComponent}
      aria-labelledby='draggable-dialog-title'
    >
      <DialogTitle className={classes.dialogTitle}>
        Stream Wizard
      </DialogTitle>
      <DialogContent className={classes.mainContainer}>
        <Grid container className={classes.formContainer}>
          <Grid container xs={12} className={classes.gridItem}>
            <Grid item xs={2}>
              <Typography variant='h5'>Charge Category</Typography>
            </Grid>
            <Grid item xs={4}>
              <FormControl
                variant='outlined'
                fullWidth
                className={classes.textField}
              >
                <Select
                  name='ChargeCategory'
                  onChange={textHandler}
                  value={stateValues.ChargeCategory}
                  //value={developer}
                >
                  {['Charge Category 1', 'Charge Category 2', 'Charge Category 3'].map((dev) => (
                    <MenuItem value={dev}>{dev}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={2}>
              <Typography variant='h5' style={{ marginLeft: '20px' }}>
                Charge Frequency
              </Typography>
            </Grid>
            <Grid item xs={4}>
              <FormControl
                variant='outlined'
                fullWidth
                className={classes.textField}
              >
                <Select
                  name='ChargeFrequency'
                  onChange={textHandler}
                  value={stateValues.ChargeFrequency}
                >
                  {['Charge Frequency 1', 'Charge Frequency 2', 'Charge Frequency 3'].map((dev) => (
                    <MenuItem value={dev}>{dev}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
          </Grid>

          <Grid container xs={12} className={classes.gridItem}>
            <Grid item xs={2}>
              <Typography variant='h5'>APN Names</Typography>
            </Grid>
            <Grid item xs={10}>
              <FormControl fullWidth className={classes.textField}>
                <TextField
                  name='ApplicationName'
                  variant='outlined'
                  placeholder='Enter APN Name'
                  onChange={textHandler}
                  value={stateValues.ApplicationName}
                />
              </FormControl>
            </Grid>
          </Grid>
        

          {/* 2nd Step */}
          <Grid container xs={12} className={classes.gridItem}>
            <Grid item xs={2}>
              <Typography variant='h5'>Traffic Classification Criteria</Typography>
            </Grid>
            <Grid item xs={10} className={classes.checkboxContainer}>
              <FormControl component='fieldset' className={classes.formControl}>
                <FormGroup>
                  <FormControlLabel
                    control={<Checkbox name='gilad' />}
                    label='HTTP URL or HTTPS Domain'
                  />
                  <FormControlLabel
                    control={<Checkbox name='jason' />}
                    label='Jason Killian'
                  />
                  <FormControlLabel
                    control={<Checkbox name='antoine' />}
                    label='Antoine Llorca'
                  />
                </FormGroup>
              </FormControl>
              <FormControl
                required
                component='fieldset'
                className={classes.formControl}
              >
                <FormGroup>
                  <FormControlLabel
                    control={<Checkbox name='gilad' />}
                    label='HTTP URL or HTTPS Domain'
                  />
                  <FormControlLabel
                    control={<Checkbox name='jason' />}
                    label='Jason Killian'
                  />
                  <FormControlLabel
                    control={<Checkbox name='antoine' />}
                    label='Antoine Llorca'
                  />
                </FormGroup>
              </FormControl>
            </Grid>
          </Grid>
          <Grid container xs={12} className={classes.gridItem}>
            <Grid item xs={2}>
              <Typography variant='h5'>Preferred Stream Name</Typography>
            </Grid>
            <Grid item xs={10}>
              <FormControl fullWidth className={classes.textField}>
                <TextField
                  name='StreamName'
                  variant='outlined'
                  placeholder='Enter Preferred Stream Name'
                  onChange={textHandler}
                  value={stateValues.StreamName}
                />
              </FormControl>
            </Grid>
          </Grid>

          <Grid container xs={12} className={classes.gridItem}>
            <Grid item xs={2}>
              <Typography variant='h5'>Traffic Identifier</Typography>
            </Grid>
            <Grid item xs={4}>
            <TextField
                  name='TrafficIdentifier'
                  variant='outlined'
                  placeholder='Enter Traffic Identfier'
                  onChange={textHandler}
                  value={stateValues.TrafficIdentifier}
                  fullWidth
                />
                 
                
            </Grid>
            <Grid item xs={2}>
              <Typography variant='h5' style={{ marginLeft: '20px' }}>
                Stream ID
              </Typography>
            </Grid>
            <Grid item xs={4}>
            <TextField
                  name='StreamId'
                  variant='outlined'
                  placeholder='Enter Stream ID'
                  fullWidth
                  onChange={textHandler}
                  value={stateValues.StreamId}
                  inputProps={{
                    readOnly: props.formType === 2 ? true : false
                  }}
                />
            </Grid>
          </Grid>


          <Grid container xs={12} className={classes.gridItem}>
            <Grid item xs={2}>
              <Typography variant='h5'>Test Criteria(If Any)</Typography>
            </Grid>
            <Grid item xs={10}>
            <TextField
                  name='TestCriteria'
                  variant='outlined'
                  placeholder='Test Criteria'
                  onChange={textHandler}
                  value={stateValues.TestCriteria}
                  fullWidth
                />
            </Grid>
          </Grid>

          <Grid container xs={12} className={classes.gridItem}>
            <Grid item xs={2}>
              <Typography variant='h5'>Special Notes</Typography>
            </Grid>
            <Grid item xs={10}>
            <TextField
                  name='Note'
                  variant='outlined'
                  onChange={textHandler}
                  value={stateValues.Note}
                  placeholder='Notes'
                  fullWidth
                />
            </Grid>
          </Grid>


         
          {/* 2nd Step End */}
        </Grid>
      
      </DialogContent>

      <DialogActions className={classes.modalFooter}>
     
          <Button
            autoFocus
            onClick={props.handleClose}
            className={classes.cancelButton}
          >
            Cancel
          </Button>
          <Button className={classes.nextButton} onClick={saveHandler}>Save Stream ID</Button>
        
      </DialogActions>
    </Dialog>
  )
}

export default AddStream

import React, { useState, useEffect } from 'react'
// import { makeStyles } from '@material-ui/core/styles';
import {
  Typography,
  Grid,
  TextField,
  Button,
  Dialog,
  DialogContent,
  DialogActions,
  DialogTitle,
  makeStyles,
  FormControl,
  Select,
  MenuItem,
  CircularProgress,
} from '@material-ui/core'
import {
  USER_SIGN_UP,
  GET_USER_ID,
  UPDATE_USER,
  GET_ALL_USERS,
  GET_ALL_USERSGROUPS
} from '../shared/constants'
import { globalStyles } from '../globalstyles'
import { useMutation, useQuery } from '@apollo/client'
import { useToasts } from 'react-toast-notifications'

const useStyles = makeStyles((theme) => ({
  mainContainer: {},
  formContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    padding: theme.spacing(2, 5, 5, 5),
    // paddingLeft: 100,
    // paddingRight: 100,
    // '& .MuiTextField-root': {
    //   margin: theme.spacing(1),
    //   width: '300px',
    // },
    // '& .MuiButtonBase-root': {
    //   margin: theme.spacing(2),
    // },
  },
  textField: {
    '& .MuiTextField-root': {
      width: '100%',
      margin: 0,
    },
    '& .MuiOutlinedInput-root': {
      backgroundColor: 'rgba(82, 107, 198, 0.32)',
    },
  },
  gridItem: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: '10px 0',
    '& .MuiGrid-grid-xs-2 h5':{
      //marginLeft: '50px'
    }
  },
  stepper: {
    '& .MuiStepLabel-labelContainer span': {
      fontSize: '24px',
    },
    padding: theme.spacing(5, 0, 5, 0),
  },
  dialogTitle: {
    backgroundColor: '#4b286d',
    color: '#fff',
  },
  modalFooter: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: theme.spacing(4),
    '& .MuiButton-root': {
      color: '#fff',
      boxShadow: 'none',
      textTransform: 'capitalize',
      padding: '10px 30px',
    },
    '& .MuiButton-label': {
      fontSize: '16px',
    },
    // '& .MuiButton-root.Mui-disabled': {
    //   backgroundColor: '#ffffff !important',
    // },
  },
  cancelButton: {
    '&.MuiButton-root': {
      backgroundColor: '#BDBDBD',
    },
  },
  nextButton: {
    '&.MuiButton-root': {
      backgroundColor: '#000',
    },
  },
  asterikClass: {
    color: 'red',
  },
  errorStyle: {
    //marginBottom: '10px'
  },
}))

const AddUser = (props) => {
  const classes = useStyles()
  const globalClasses = globalStyles()
  const user = JSON.parse(localStorage.getItem('user'))
  const { addToast } = useToasts()

  const emailRegex =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  const phoneRegex = new RegExp(/^[0-9\b]+$/)
  const [allUserGroups, setAllUSerGroups] = useState([])
  const [stateValues, setStateValues] = useState({
    Username: '',
    Firstname: '',
    Lastname: '',
    Email: '',
    ContactNo: '',
    IsActive: '',
    roles: '',
    UserGroup: ''
    
    // userstatus: false
  })
  const [stateValuesError, setStateValuesError] = useState({
    Username: '',
    Firstname: '',
    Lastname: '',
    Email: '',
    ContactNo: '',
    IsActive: '',
    roles: '',
    UserGroup: ''
    // useraccess: '',
    // userstatus: false
  })
  const { loading:loadingUsrGr, data:dataUsrGr, error:errorUsrGr } = useQuery(GET_ALL_USERSGROUPS, {
    onError: () => {
      
     },
  })
  const [addUser, { loading, error, data }] = useMutation(USER_SIGN_UP, {
    variables: {
      createUsersInput: {
        Username: stateValues.Username,
        Firstname: stateValues.Firstname,
        Lastname: stateValues.Lastname,
        Email: stateValues.Email,
        ContactNo: stateValues.ContactNo,
        //Image: profileImagePath
      },
    },
    onError: () => {
      //Do nothing
    },
    onCompleted: () => {
      addToast('User Created Successfully', { appearance: 'success' })
      //history.push("/welcomepage")
      props.handleClose()
    },
    refetchQueries: [{ query: GET_ALL_USERS }],
    awaitRefetchQueries: true,
  })

  const {
    loading: loadingUser,
    data: dataUser,
    error: errorUser,
  } = useQuery(GET_USER_ID, {
    variables: { id: props && props.userId },
    skip: props.formType === 1,
    onError: () => {
      props.handleClose()
    },
  })

  // const [updateUserMutation, { loading:loadingUpdate, data:dataUpdate, error:errorUpdate,  }] = useMutation(UPDATE_USER, {
  //   variables: {
  //     updateUserInput: {
  //       Firstname: stateValues.firstname,
  //       Lastname: stateValues.lastname,
  //       Email: stateValues.email,
  //       ContactNo: stateValues.contactno
  //     },
  //     id: props && props.userId,
  //   },
  //   skip: props.formType === 1,
  //   refetchQueries: [{ query: GET_ALL_USERS }],
  //   awaitRefetchQueries: true,
  //   onError: () => {
  //     //do nothing
  //   },
  //   onCompleted: () => {
  //     addToast("Annoucement Created Successfully", { appearance: "success" });
  //     console.log('complete')
  //     //history.push("/adminlanding")
  //   },

  // }
  // );

  const [
    updateUserMutation,
    { loading: loadingUpdate, data: dataUpdate, error: errorUpdate },
  ] = useMutation(UPDATE_USER, {
    onCompleted: () => {
      addToast('User saved Successfully', { appearance: 'success' })
      props.handleClose()
      console.log('completed')
    },
    variables: {
      updateUserInput: {
        Firstname: stateValues.firstname,
        Lastname: stateValues.lastname,
        Email: stateValues.email,
        ContactNo: stateValues.contactno,
      },
      id: props && props.userId,
    },
    //refetchQueries: [{ query: GET_ALL_USERS }],
    //refetchQueries: [{ query: GET_USER_ID, variables: { id: props && props.userId }  }],
    //awaitRefetchQueries: true,
    update: (cache) => {
      cache.evict({
        id: 'ROOT_QUERY',
        field: 'id',
      })
    },
    onError: () => {
      //Do nothing
    },
  })

  useEffect(() => {
    if (props.formType === 2) {
      if (dataUser) {
        console.log('data', dataUser)
        setStateValues((prevState) => ({
          ...prevState,
          Firstname: dataUser.UsersById.Firstname,
        }))
        setStateValues((prevState) => ({
          ...prevState,
          Lastname: dataUser.UsersById.Lastname,
        }))
        setStateValues((prevState) => ({
          ...prevState,
          Email: dataUser.UsersById.Email,
        }))
        setStateValues((prevState) => ({
          ...prevState,
          ContactNo: dataUser.UsersById.ContactNo,
        }))
      }
    }

    if (dataUpdate) {
      console.log('data', dataUpdate)
    }
    if(dataUsrGr){
      console.log('data', dataUsrGr)
      setAllUSerGroups(dataUsrGr.getAllUserGroups)
    }
  }, [dataUser, dataUpdate, dataUsrGr])

  const textHandler = (e) => {
    const { name, value } = e.target
    console.log('name', name, value)
    setStateValues({ ...stateValues, [name]: value })
    setStateValuesError({ ...stateValuesError, [name]: '' })

    if(name === 'usergroup'){
      const filteredGroup = allUserGroups.filter(usrg => usrg.id === value)
      setStateValues({ ...stateValues, [name]: {id: value, GroupName: filteredGroup[0].GroupName} })
      setStateValuesError({ ...stateValuesError, [name]: '' })
    }
  }

  const saveHandler = async () => {
    let pErr = false

    // let pErr = false;
    if (props.formType === 1) {
      if (stateValues.Username === '') {
        setStateValuesError((prevState) => ({
          ...prevState,
          Username: 'Please enter user name',
        }))
        pErr = true
      }
    }

    if (!emailRegex.test(String(stateValues.Email).toLowerCase())) {
      setStateValuesError((prevState) => ({
        ...prevState,
        Email: 'Please enter valid email',
      }))
      pErr = true
    }
    //   if (!phoneRegex.test(String(stateValues.contactno))) {
    //     setStateValuesError((prevState) => ({ ...prevState, contactno: 'Please enter contact no', }));
    //     pErr = true
    // }
    // if (stateValues.firstname === "") {
    //     setStateValuesError((prevState) => ({ ...prevState, firstname: 'Please enter fistname', }));
    //     pErr = true
    // }
    // if (!stateValues.lastname) {
    //     setStateValuesError((prevState) => ({ ...prevState, lastname: 'Please enter lastname', }));
    //     pErr = true
    // }
     if (stateValues.roles === "") {
         setStateValuesError((prevState) => ({ ...prevState, roles: 'Please select useraccess level', }));
         pErr = true
     }
     if (typeof stateValues.IsActive !== 'boolean' ) {
      setStateValuesError((prevState) => ({ ...prevState, IsActive: 'Please select userastatus level', }));
      pErr = true
    }
    // if (stateValues.useraccess === "") {
    //   setStateValuesError((prevState) => ({ ...prevState, useraccess: 'Please select useraccess level', }));
    //   pErr = true
    // }

    if (props && props.formType === 2) {
      if (stateValues.Email && !pErr) {
        console.log('update')
        await updateUserMutation()
      }
    }

    if (props && props.formType === 1) {
      if (stateValues.Email && !pErr) {
        console.log('create', stateValues)
        //await addUser()
      }
    }

    //console.log('stateValues', stateValues, stateValuesError)

    // if (stateValues.usergroup === "") {
    //     setStateValuesError((prevState) => ({ ...prevState, usergroup: 'Please enter user group', }));
    //     pErr = true
    // }
    // if (stateValues.useraccess === "") {
    //     setStateValuesError((prevState) => ({ ...prevState, useraccess: 'Please enter user access', }));
    //     pErr = true
    // }
    // if (!stateValues.userstatus) {
    //     setStateValuesError((prevState) => ({ ...prevState, userstatus: 'Please enter user status', }));
    //     pErr = true
    // }
  }

  return (
    <Dialog
      open={props.open}
      onClose={props.handleClose}
      maxWidth='lg'
      fullWidth
      //PaperComponent={PaperComponent}
      aria-labelledby='draggable-dialog-title'
    >
      <DialogTitle className={classes.dialogTitle}>
        <Typography variant='h6'>User Wizard</Typography>
      </DialogTitle>

      <DialogContent className={classes.mainContainer}>
        <Grid container className={classes.formContainer}>
          <Grid container xs={12} className={classes.gridItem}>
            <Grid item xs={2}>
              <Typography variant='h5'>First Name</Typography>
            </Grid>

            <Grid item xs={4}>
              <FormControl fullWidth className={classes.textField}>
                <TextField
                  name='Firstname'
                  variant='outlined'
                  placeholder='First Name'
                  onChange={textHandler}
                  //size="small"
                  value={stateValues.Firstname}
                  inputProps={{
                    maxLength: 500,
                  }}
                />
              </FormControl>
              {/* <Typography
                className={classes.errorStyle}
                component="p"
                color="error"
                variant="body2"
              >
               {stateValuesError.lastname}
              </Typography> */}
            </Grid>

            <Grid item xs={2}>
              <Typography variant='h5' style={{ marginLeft: '50px' }}>
                Last Name
              </Typography>
            </Grid>

            <Grid item xs={4}>
              <FormControl fullWidth className={classes.textField}>
                <TextField
                  name='Lastname'
                  variant='outlined'
                  placeholder='Last Name'
                  onChange={textHandler}
                  value={stateValues.Lastname}
                  inputProps={{
                    maxLength: 500,
                  }}
                />
              </FormControl>
              {/* <Typography
                className={classes.errorStyle}
                component="p"
                color="error"
                variant="body2"
              >
                {stateValuesError.lastname}
              </Typography> */}
            </Grid>
          </Grid>

          {/* {props.formType === 1 && (
            <Grid container xs={12} className={classes.gridItem}>
              <Grid item xs={2}>
                <Typography variant='h5'>
                  User Name<span className={classes.asterikClass}>*</span>
                </Typography>
              </Grid>
              <Grid item xs={4}>
                <FormControl fullWidth className={classes.textField}>
                  <TextField
                    name='username'
                    variant='outlined'
                    placeholder='User Name'
                    onChange={textHandler}
                    value={stateValues.username}
                    inputProps={{
                      maxLength: 500,
                    }}
                  />
                </FormControl>
                <Typography
                  className={classes.errorStyle}
                  component='p'
                  color='error'
                  variant='body2'
                >
                  {stateValuesError.username}
                </Typography>
              </Grid>
            </Grid>
          )} */}

          <Grid container xs={12} className={classes.gridItem}>
            <Grid item xs={2}>
              <Typography variant='h5'>
                User Name<span className={classes.asterikClass}>*</span>
              </Typography>
            </Grid>

            <Grid item xs={4}>
              <FormControl fullWidth className={classes.textField}>
                <TextField
                  name='Username'
                  variant='outlined'
                  placeholder='User Name'
                  onChange={textHandler}
                  value={stateValues.Username}
                  inputProps={{
                    maxLength: 500,
                  }}
                />
              </FormControl>
              <Typography
                className={classes.errorStyle}
                component='p'
                color='error'
                variant='body2'
              >
                {stateValuesError.Username}
              </Typography>
            </Grid>

            <Grid item xs={2}>
              <Typography variant='h5' style={{ marginLeft: '50px' }}>
                Email<span className={classes.asterikClass}>*</span>
              </Typography>
            </Grid>

            <Grid item xs={4}>
              <FormControl fullWidth className={classes.textField}>
                <TextField
                  name='Email'
                  variant='outlined'
                  placeholder='example@example.com'
                  onChange={textHandler}
                  value={stateValues.Email}
                  inputProps={{
                    maxLength: 500,
                    readOnly: props.formType === 2 ? true : false,
                  }}
                />
              </FormControl>
              <Typography
                className={classes.errorStyle}
                component='p'
                color='error'
                variant='body2'
              >
                {stateValuesError.Email}
              </Typography>
            </Grid>
          </Grid>

          <Grid container xs={12} className={classes.gridItem}>
            <Grid item xs={2}>
              <Typography variant='h5'>Access Level<span className={classes.asterikClass}>*</span></Typography>
            </Grid>

            <Grid item xs={4}>
            <FormControl
                variant="outlined"
                fullWidth
                className={classes.textField}
              >
                <Select
                  name="roles"
                  value={stateValues.roles}
                  onChange={textHandler}
                >
                  {[{id: 1, status: 'Contributor '}, {id: 2, status: 'Admin'}, {id: 3, status: 'Viewer'}].map((dev) => (
                    <MenuItem value={dev.id}>{dev.status}</MenuItem>
                  ))}
                </Select>
              </FormControl>
              <Typography
                className={classes.errorStyle}
                component="p"
                color="error"
                variant="body2"
              >
                {stateValuesError.roles}
              </Typography>
            </Grid>

            <Grid item xs={2}>
              <Typography variant='h5' style={{ marginLeft: '50px' }}>
                Contact No
              </Typography>
            </Grid>

            <Grid item xs={4}>
              <FormControl fullWidth className={classes.textField}>
                <TextField
                  name='ContactNo'
                  variant='outlined'
                  placeholder='12345678'
                  onChange={textHandler}
                  value={stateValues.ContactNo}
                  inputProps={{
                    maxLength: 500,
                  }}
                />
              </FormControl>
              {/* <Typography
                className={classes.errorStyle}
                component="p"
                color="error"
                variant="body2"
              >
                {stateValuesError.contactno}
              </Typography> */}
            </Grid>
          </Grid>

          {/* Not reuquired till now */}

          <Grid container xs={12} className={classes.gridItem}>
            <Grid item xs={2}>
              <Typography variant="h5">User Group<span className={classes.asterikClass}>*</span></Typography>
            </Grid>
            <Grid item xs={4}>
              <FormControl
                variant="outlined"
                fullWidth
                //className={classes.textField}
              >
                <Select
                  name="UserGroup"
                  onChange={textHandler}
                  value={stateValues.UserGroup && stateValues.UserGroup.id}
                >
                  {allUserGroups.map((dev) => (
                    <MenuItem value={dev.id}>{dev.GroupName}</MenuItem>
                  ))}
                </Select>
              </FormControl>
              <Typography
                className={classes.errorStyle}
                component="p"
                color="error"
                variant="body2"
              >
                {stateValuesError.UserGroup}
              </Typography>
            </Grid>
            <Grid item xs={2}>
              <Typography variant="h5" style={{ marginLeft: '50px' }}>Status<span className={classes.asterikClass}>*</span></Typography>
            </Grid>
            <Grid item xs={4}>
              <FormControl
                variant="outlined"
                fullWidth
                className={classes.textField}
              >
                <Select
                  name="IsActive"
                  value={stateValues.IsActive}
                  onChange={textHandler}
                >
                  {[{id: true, status: 'Active'}, {id: false, status: 'In Active'}].map((dev) => (
                    <MenuItem value={dev.id}>{dev.status}</MenuItem>
                  ))}
                </Select>
              </FormControl>
              <Typography
                className={classes.errorStyle}
                component="p"
                color="error"
                variant="body2"
              >
                {stateValuesError.IsActive}
              </Typography>
           
            </Grid>
          </Grid>

          {/* <Grid container xs={12} className={classes.gridItem}>
            <Grid item xs={3}>
              <Typography variant="h5">Access Level</Typography>
            </Grid>
            <Grid item xs={9}>
              <FormControl
                variant="outlined"
                fullWidth
                
                className={classes.textField}
              >
                <Select
                 name="useraccess"
                  value={stateValues.useraccess}
                  onChange={textHandler}
                >
                  {['Administrator', 'User', 'Viewer'].map((dev) => (
                    <MenuItem value={dev}>{dev}</MenuItem>
                  ))}
                </Select>
              </FormControl>
              <Typography
                className={classes.errorStyle}
                component="p"
                color="error"
                variant="body2"
              >
                {stateValuesError.useraccess}
              </Typography>
            </Grid>
          </Grid> */}

          {/* <Grid container xs={12} className={classes.gridItem}>
            <Grid item xs={3}>
              <Typography variant="h5">Status<span className={classes.asterikClass}>*</span></Typography>
            </Grid>
            <Grid item xs={9}>
           
            
              <FormControl
                variant="outlined"
                fullWidth
                className={classes.textField}
              >
                <Select
                  name="userstatus"
                  value={stateValues.stateValues}
                  onChange={textHandler}
                >
                  {[{id: 1, status: 'Active'}, {id: 0, status: 'In Active'}].map((dev) => (
                    <MenuItem value={dev.id}>{dev.status}</MenuItem>
                  ))}
                </Select>
              </FormControl>
              <Typography
                className={classes.errorStyle}
                component="p"
                color="error"
                variant="body2"
              >
                {stateValuesError.userstatus}
              </Typography>
           
            </Grid>
          </Grid> */}
        </Grid>
        {(loading || loadingUser || loadingUpdate) && (
          <div className={globalClasses.loadingIndicator}>
            <CircularProgress />
          </div>
        )}
      </DialogContent>

      <DialogActions className={classes.modalFooter}>
        <Button
          autoFocus
          onClick={props.handleClose}
          className={classes.cancelButton}
        >
          Cancel
        </Button>
        <Button className={classes.nextButton} onClick={saveHandler}>
          Submit
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default AddUser

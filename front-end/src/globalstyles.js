import { makeStyles } from '@material-ui/core'

export const globalStyles = makeStyles((theme) => ({
  loadingIndicator: {
    '& .MuiCircularProgress-colorPrimary': {
      color: 'green',
    },
    position: 'absolute',
    top: '50%',
    left: '50%',
    color: 'white',
    MozTransform: 'translateX(-50%) translateY(-50%)',
    WebkitTransform: 'translateX(-50%) translateY(-50%)',
    transform: 'translateX(-50%) translateY(-50%)',
  },
  buttonStyle: {
    '&.MuiButton-root': {
      color: '#fff',
      boxShadow: 'none',
      textTransform: 'capitalize',
      padding: '10px 15px',
      backgroundColor: 'green',
    },
    '&.MuiIconButton-root': {
      fontSize: '1rem',
    },
  },
  container: {
    padding: theme.spacing(10, 5, 10, 5),
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    //alignItems: 'center',
  },
  headerButtons: {
    //display: 'flex',
    //justifyContent: 'space-between',
    //padding: theme.spacing(2),
    '& .MuiButton-root': {
      color: '#fff',
      boxShadow: 'none',
      textTransform: 'capitalize',
      //padding: '5px 15px',
      backgroundColor: '#000',
    },
    '& .MuiIconButton-root': {
      fontSize: '1rem',
    },
    // '& .MuiButton-label': {
    //     fontSize: '12px'
    // },
    // '& .MuiButton-root.Mui-disabled': {
    //   backgroundColor: '#ffffff !important',
    // },
    '& > *': {
      margin: '0 10px',
    },
  },
  iconButtonStyle: {
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
  formContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    padding: theme.spacing(2, 5, 5, 5),
    // paddingLeft: 100,
    // paddingRight: 100,
    // '& .MuiTextField-root': {
    //   margin: theme.spacing(1),
    //   width: '300px',
    // },
    // '& .MuiButtonBase-root': {
    //   margin: theme.spacing(2),
    // },
  },
  textField: {
    '& .MuiTextField-root': {
      width: '100%',
      margin: 0,
    },
    '& .MuiOutlinedInput-root': {
      backgroundColor: 'rgba(82, 107, 198, 0.32)',
    },
  },
  gridItem: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: '10px 0',
  },
  modalFooter: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: theme.spacing(4),
    '& .MuiButton-root': {
      color: '#fff',
      boxShadow: 'none',
      textTransform: 'capitalize',
      padding: '10px 30px',
    },
    '& .MuiButton-label': {
      fontSize: '16px',
    },
    // '& .MuiButton-root.Mui-disabled': {
    //   backgroundColor: '#ffffff !important',
    // },
  },
  cancelButton: {
    '&.MuiButton-root': {
      backgroundColor: '#BDBDBD',
    },
  },
  nextButton: {
    '&.MuiButton-root': {
      backgroundColor: '#000',
    },
  },
  asterikClass: {
    color: 'red',
  },
  paginationIcons:{
    '& .MuiSvgIcon-root':{
      fill: 'black'
    }
  }
  
}))

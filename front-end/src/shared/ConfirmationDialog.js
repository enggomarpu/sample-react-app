import {
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    Button,
    Box,
    IconButton,
    Typography,
    CircularProgress,
} from '@material-ui/core';
import { Close } from '@material-ui/icons';

const ConfirmDialog = (props) => {
    return (
        <Dialog open={props.open} maxWidth="sm" fullWidth>
            <DialogTitle>{props.showConfirm ? 'Confirm the action' : 'Oops!'}</DialogTitle>
            <Box position="absolute" top={0} right={0}>
                <IconButton onClick={props.handleClose}>
                    <Close />
                </IconButton>
            </Box>
            <DialogContent>
                <Typography>{props.message}</Typography>
            </DialogContent>
            <DialogActions>
                <Button color="primary" variant="contained" onClick={props.handleClose}>
                    {props.showConfirm ? 'Cancel' : 'Close'}
                </Button>
                {props.showConfirm && <Button disabled={props.loadingInd} color="secondary" variant="contained" onClick={props.confirmFunc}>
                    {props.loadingInd ? <CircularProgress size={25} /> : 'Confirm'}

                </Button>}
            </DialogActions>
        </Dialog>
    );
};

export default ConfirmDialog;
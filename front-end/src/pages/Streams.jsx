import React, { useState, useEffect } from 'react'
import {
  Button,
  IconButton,
  makeStyles,
  Typography,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  CircularProgress,
  Radio,
  TablePagination,
  TableSortLabel,
  TextField,
} from '@material-ui/core'
import SortIcon from '@material-ui/icons/Sort'
import FilterIcon from '@material-ui/icons/Filter'
import EditIcon from '@material-ui/icons/Edit'
import { globalStyles } from '../globalstyles'
import { GET_ALL_STREAMS, GET_ALL_STREAM_CUSTOMERS } from '../shared/constants'
import { useQuery } from '@apollo/client'
import moment from 'moment'
import AddStream from '../components/AddStream'
import SearchIcon from '@material-ui/icons/Search'
import StreamLogModel from '../components/StreamLogModel'
import { CsvBuilder } from 'filefy'

const useStyles = makeStyles((theme) => ({
  container: {
    padding: theme.spacing(10, 5, 10, 5),
    '& > *': {
      //padding: '10px 30px'
    },
  },
  header: {
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottom: '1px solid red',
  },
  headerButtons: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: theme.spacing(2),
    '& .MuiButton-root': {
      color: '#fff',
      boxShadow: 'none',
      textTransform: 'capitalize',
      padding: '5px 15px',
      backgroundColor: '#000',
    },
    '& .MuiButton-label': {
      fontSize: '12px',
    },
    // '& .MuiButton-root.Mui-disabled': {
    //   backgroundColor: '#ffffff !important',
    // },
    '& > *': {
      margin: '0 10px',
    },
  },
  iconButtonStyle: {
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
  tableContainer: {},
  tableHeaderBottomBorder: {
    '& .MuiTableCell-root': {
      borderBottom: '1px solid black',
    },
    '& .MuiTableCell-head': {
      color: 'black',
      fontSize: '16px',
      paddingTop: 7.5,
      paddingBottom: 7.5,
    },
  },
  iconTableEditButton: {
    '&.MuiIconButton-root': {
      padding: 0,
    },
  },
  tableBodyBottomBorder: {
    '& .MuiTableCell-root': {
      borderBottom: 'none',
    },
    '& .MuiTableCell-body': {
      color: 'black',
      fontSize: '16px',
      paddingTop: 10,
      paddingBottom: 10,
    },
    margin: 0,
  },
  tableFirstColumnText: {
    '&:first-child': {
      color: '#56CCF2',
    },
    width: '25%',
  },
}))

const Streams = () => {
  const classes = useStyles()
  const globalClasses = globalStyles()
  const [openStreamSignupDialog, setOpenStreamSignupDialog] = useState(false)
  const [formType, setFormType] = useState(1)
  const [openLogModel, setOpenLogModel] = useState(false)
  const [streamId, setStreamId] = useState()
  const [allStreams, setAllStreams] = useState([])
  const [allStreamCustomers, setAllStreamCustomers] = useState([])
  const [selected, setSelected] = React.useState([]);
  const [streamName, setStreamName] = useState('')
  const user = JSON.parse(localStorage.getItem('user'))
  
  const [customerPage, setCustomerPage] = React.useState(0);
  const [customerRowsPerPage, setCustomerRowsPerPage] = React.useState(5);

  const [order, setOrder] = useState('asc');
  const [orderBy, setOrderBy] = useState('RatingGroupId');

  
  const [customerStreamsPage, setCustomerStreamsPage] = React.useState(0);
  const [customerStreamsRowsPerPage, setCustomerStreansRowsPerPage] = React.useState(5);
  const [filterFn, setFilterFn] = useState({ fn: items => { return items; } })


  const columns = [
    { label: "Rating Group ID", field: "RatingGroupId", align: "" },
    { label: "Service ID", field: "ServiceId", align: "left" },
    { label: "Stream ID", field: "StreamId", align: "left" },
    { label: "Stream Name", field: "StreamName", align: "left" },
    { label: "Application Name", field: "ApplicationName", align: "left" },
    { label: "Charge Category", field: "ChargeCategory", align: "left" },
  ]
  
  const customerColumns = [
    { label: "Customers", field: "CustomerName", align: "" },
    { label: "Opportunities", field: "Opportunity", align: "left" },
    { label: "Opportunity Type", field: "OpportunityType", align: "left" },
    { label: "Business Unit", field: "BusinessUnit", align: "left" },
    { label: "Updated", field: "UpdatedDate", align: "left" },
  ]

  const { loading, data, error } = useQuery(GET_ALL_STREAMS, {
    skip: false,
    onError: () => { },
  })

  const { loading: customerLoading, data: customerData, refetch } = useQuery(GET_ALL_STREAM_CUSTOMERS, {
    variables: {
      id: streamId && streamId
    },
    skip: !streamId,
    onError: () => { },
  })

  const openStreamLog = (id, name) => {
    setOpenLogModel(true)
    setStreamId(id)
    setStreamName(name)
  }

  const closeLogModel = () => {
    setOpenLogModel(false)
  }

  const handleCustomerChangePage = (event, newPage) => {
    setCustomerPage(newPage);
  };

  const handleCustomerChangeRowsPerPage = (event) => {
    setCustomerRowsPerPage(parseInt(event.target.value, 10));
    setCustomerPage(0);
  };

  
  const handleCustomerStreamsChangePage = (event, newPage) => {
    setCustomerStreamsPage(newPage);
  };

  const handleCustomerStreamsChangeRowsPerPage = (event) => {
    setCustomerStreansRowsPerPage(parseInt(event.target.value, 10));
    setCustomerStreamsPage(0);
  };

  const handleRequestSort = (property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const descendingComparator = (a, b, orderBy) => {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  }

  const getComparator = (order, orderBy) => {
    return order === 'desc'
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
  }

  const stableSort = (array, comparator) => {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
  }

  useEffect(() => {
    if (data) {
      setAllStreams(data.getAllStreams);
      if (data.getAllStreams.length > 0) {
        setSelected(data.getAllStreams[0].id);
        setStreamId(data.getAllStreams[0].id);
      }
    }
  }, [data]);

  useEffect(() => {
    if (customerData) {
      setAllStreamCustomers(customerData.getAllStreamCustomers)
    }
  }, [customerData])

  useEffect(() => {
    if (streamId) {
      refetch()
    }
  }, [streamId])

  const editDialog = (id) => {
    setOpenStreamSignupDialog(true)
    setStreamId(id)
    setFormType(2)
  }

  const handleChangeSelect = (id) => {
    setSelected(id)
    setStreamId(id);
  }

  const exportCSV = () => {
    var csvBuilder = new CsvBuilder("streams_list.csv")
      .setColumns(columns.map(x => x.header))
      .addRows(allStreams.map(x => [x.RatingGroupId, x.ServiceId, x.StreamId, x.StreamName, x.ApplicationName, x.ChargeCategory]))
      .exportFile();
  }

  const isSelected = (id) => selected.indexOf(id) !== -1;
  const handleSearch = e => {
    let target = e.target;
    setFilterFn({
        fn: allUsers => {
            if (target.value === "")
                return allUsers;
            else
                return allUsers.filter(x => x.Email.toLowerCase().includes(target.value.toLowerCase()))
        }
    })
  }
  return (
    <>
      <div className={classes.container}>
        <header className={globalClasses.header}>
          <Typography variant='h6'>Stream IDs</Typography>
          <div className={globalClasses.headerButtons}>
           <TextField variant="outlined" onChange={handleSearch} />
            <IconButton className={globalClasses.iconButtonStyle}>
              <SortIcon style={{ margin: '0 5px' }} /> Sort
            </IconButton>
            <IconButton className={globalClasses.iconButtonStyle}>
              <FilterIcon style={{ margin: '0 5px' }} /> Filter
            </IconButton>
            <Button className='btn-primary' disableTouchRipple disableFocusRipple onClick={exportCSV}>
              Export to CSV
            </Button>
          </div>
        </header>

        <TableContainer
          className={`${classes.tableContainer} table`}
          style={{ marginBottom: '100px' }}
        >
          <Table sx={{ minWidth: 650 }} aria-label='simple table'>
            <TableHead>
              <TableRow className={classes.tableHeaderBottomBorder}>
                  {columns.map((item) => {
                      return (
                        <TableCell
                          key={item.field}
                          sortDirection={orderBy === item.field ? order : false}
                        >
                          <TableSortLabel
                            active={orderBy === item.field}
                            direction={orderBy === item.field ? order : 'asc'}
                            onClick={() => handleRequestSort(item.field)}
                          >
                            {item.label}
                          </TableSortLabel>
                        </TableCell>
                      )
                    })}
                <TableCell align='left'>Audit</TableCell>
                {(user.roles === 2 || user.roles === 1) && <TableCell align='left'></TableCell>}
              </TableRow>
            </TableHead>
            <TableBody>
              {loading ? (
                <div className={globalClasses.loadingIndicator}>
                  <CircularProgress />
                </div>
              ) : allStreams.length > 0 ? (
                stableSort(filterFn.fn(allStreams), getComparator(order, orderBy)).slice(customerStreamsPage * customerStreamsRowsPerPage, customerStreamsPage * customerStreamsRowsPerPage + customerStreamsRowsPerPage).map((stream, index) => {
                  const labelId = `enhanced-table-checkbox-${index}`;
                  const isItemSelected = isSelected(stream.id);
                  return (<TableRow
                    hover
                    onClick={() => handleChangeSelect(stream.id)}
                    role="checkbox"
                    aria-checked={isItemSelected}
                    tabIndex={-1}
                    key={stream.id}
                    selected={isItemSelected}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell align='left'>
                      <Radio
                        checked={stream.id === streamId ? true : false}
                        inputProps={{ "aria-labelledby": labelId }}
                      />
                      {stream.RatingGroupId}
                    </TableCell>
                    <TableCell align='left'>{stream.ServiceId}</TableCell>
                    <TableCell align='left'>{stream.StreamId}</TableCell>
                    <TableCell align='left'>{stream.StreamName}</TableCell>
                    <TableCell align='left'>{stream.ApplicationName}</TableCell>
                    <TableCell align='left'>{stream.ChargeCategory}</TableCell>

                    <TableCell align='left'>
                      <IconButton
                        disableRipple
                        disableTouchRipple
                        className={classes.iconTableEditButton}
                        onClick={() => openStreamLog(stream.id, stream.StreamName)}
                      >
                        <SearchIcon />
                      </IconButton>
                    </TableCell>
                    { (user.roles === 2 || user.roles === 1) &&   
                    <TableCell align='left'>
                      <IconButton
                        className={classes.iconTableEditButton}
                        onClick={() => editDialog(stream.id)}
                      >
                        <EditIcon />
                      </IconButton>
                    </TableCell> }
                  
                  </TableRow>)
                })
              ) : (
                <div>No data from server</div>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          component="div"
          rowsPerPageOptions={[5, 10, 15]}
          className={globalClasses.paginationIcons}
          count={allStreams.length}
          page={customerStreamsPage}
          onPageChange={handleCustomerStreamsChangePage}
          rowsPerPage={customerStreamsRowsPerPage}
          onRowsPerPageChange={handleCustomerStreamsChangeRowsPerPage}
        />       
        <header className={globalClasses.header}>
          <Typography variant='h6'>Associated Customers</Typography>
          <div className={globalClasses.headerButtons}>
            <IconButton className={globalClasses.iconButtonStyle}>
              <SortIcon style={{ margin: '0 5px' }} /> Sort
            </IconButton>
            <IconButton className={globalClasses.iconButtonStyle}>
              <FilterIcon style={{ margin: '0 5px' }} /> Filter
            </IconButton>
          </div>
        </header>
        <TableContainer
          className={`${classes.tableContainer} table`}
          style={{ paddingBottom: '100px' }}
        >
          <Table sx={{ minWidth: 650 }} aria-label='simple table'>
            <TableHead>
              <TableRow className={classes.tableHeaderBottomBorder}>
                {/* <TableCell>Customers</TableCell>
                <TableCell align='left'>Opportunities</TableCell>
                <TableCell align='left'>Opportunity Type</TableCell>
                <TableCell align='left'>Business Unit</TableCell>
                <TableCell align='left'>IoT Platform</TableCell>
                <TableCell align='left'>Updated</TableCell>
                <TableCell align="left"></TableCell> */}
                 {customerColumns.map((item) => {
                      return (
                        <TableCell
                          key={item.field}
                          sortDirection={orderBy === item.field ? order : false}
                        >
                          <TableSortLabel
                            active={orderBy === item.field}
                            direction={orderBy === item.field ? order : 'asc'}
                            onClick={() => handleRequestSort(item.field)}
                          >
                            {item.label}
                          </TableSortLabel>
                        </TableCell>
                      )
                    })}
              </TableRow>
            </TableHead>
            <TableBody>
              {customerLoading ? (<div className={globalClasses.loadingIndicator}><CircularProgress />
              </div>
              ) : allStreamCustomers && allStreamCustomers.length > 0 ?
                (allStreamCustomers.slice(customerPage * customerRowsPerPage, customerPage * customerRowsPerPage + customerRowsPerPage).map((customer) => (
                  <TableRow
                    className={classes.tableBodyBottomBorder}
                    key={customer.id}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell align='left'>
                      {customer.CustomerName}
                    </TableCell>
                    <TableCell align='left'>
                      {customer.Opportunity}
                    </TableCell>
                    <TableCell align='left'>{customer.OpportunityType}</TableCell>
                    <TableCell align='left'>{customer.BusinessUnit}</TableCell>
                    {/* <TableCell align='left'>Siemens</TableCell> */}
                    <TableCell align='left'>
                      {moment(customer.UpdatedDate).fromNow()}
                    </TableCell>

                    {/* <TableCell align="left">
                          <IconButton className={classes.iconTableEditButton} onClick={() => {
                            setOpenCustomerSignupDialog(true); 
                            setFormType(2)}}>
                            <EditIcon  />
                          </IconButton>
                        </TableCell> */}
                  </TableRow>
                ))
                ) : (
                  <div>No data from server</div>
                )}

              {/* <TableCell align="right">
                    <IconButton
                      className={classes.iconEditButtonClasses}
                      //onClick = { () => { setOpenEditDialog(true); setAnnouncementId(annoucement.id) }}
                    >
                      <EditIcon />
                    </IconButton>
                    <IconButton
                      className={classes.iconDeleteButtonClasses}
                      //onClick= {() => { setDeleteDialogOpen(true); setAnnouncementId(annoucement.id) }}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </TableCell> */}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          component="div"
          rowsPerPageOptions={[5, 10, 15]}
          className={globalClasses.paginationIcons}
          count={allStreamCustomers.length}
          page={customerPage}
          onPageChange={handleCustomerChangePage}
          rowsPerPage={customerRowsPerPage}
          onRowsPerPageChange={handleCustomerChangeRowsPerPage}
        />        
      </div>
      {openStreamSignupDialog && (
        <AddStream
          open={openStreamSignupDialog}
          handleClose={() => setOpenStreamSignupDialog(false)}
          formType={formType}
          streamId={streamId}
        />
      )}
      {openLogModel && (
        <StreamLogModel open={openLogModel} handleClose={closeLogModel} streamId={streamId} streamName={streamName} />
      )}

    </>
  )
}
export default Streams

import React, { useState, useEffect } from 'react'
import Controls from '../components/controls/Control'
import { Avatar, Grid, makeStyles, Paper, Typography, TextField, FormControl } from '@material-ui/core'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button'
import { globalStyles } from '../globalstyles'
import { GET_USER_ID, UPDATE_USER } from '../shared/constants'
import { useMutation, useQuery } from '@apollo/client'
import { useToasts } from 'react-toast-notifications'

const genderItems = [
  { id: 'male', title: 'Male' },
  { id: 'female', title: 'Female' },
  { id: 'other', title: 'Other' },
]

const initialFValues = {
  id: 0,
  fullName: '',
  email: '',
  mobile: '',
  city: '',
  gender: 'male',
  departmentId: '',
  hireDate: new Date(),
  isPermanent: false,
}

const useStyles = makeStyles((theme) => ({
  appMain: {
    paddingLeft: '320px',
    width: '100%',
  },
  pageContent: {
    margin: theme.spacing(3),
    padding: theme.spacing(3),
  },
 
  root: {
    // '& .MuiFormControl-root': {
    //   width: '80%',
    //   margin: theme.spacing(1),
    // },
    height: '100%',
    display: 'flex',
    alignItems: 'center'
  },
  sideAvatar: {
    flex: '1 1 auto',
    textAlign: 'center',
    padding: '10px',
    margin: '0 20px',
  },
  logoImage: {
    margin: '20px',
    '& img': {
      width: 'auto',
      height: '100px',
    },
  },
  root: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
}))

const UpdateProfile = (props) => {

  const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  
  const [values, setValues] = useState(initialFValues)
  const [errors, setErrorValues] = useState(initialFValues)
  const classes = useStyles()
  const globalClasses = globalStyles()
  const user = JSON.parse(localStorage.getItem('user'))
  const { addToast } = useToasts()

  
  const [stateValues, setStateValues] = useState({
    username: '',
    firstname: '',
    lastname: '',
    email: '',
    contactno: '',

    // usergroup: '',
    // useraccess: '',
    // userstatus: false
  })
  const [stateValuesError, setStateValuesError] = useState({
    username: '',
    firstname: '',
    lastname: '',
    email: '',
    contactno: '',

    // usergroup: '',
    // useraccess: '',
    // userstatus: false
  })



  const { loading: loadingUser, data: dataUser, error: errorUser, } = useQuery(GET_USER_ID, {
    variables: { id: user && user.id },
    onError: () => {
      props.handleClose()
    },
  })

  const [ updateUserMutation, { loading: loadingUpdate, data: dataUpdate, error: errorUpdate }] = useMutation(UPDATE_USER, {
    onCompleted: () => {
      addToast('User saved Successfully', { appearance: 'success' })
      console.log('completed')
    },
    variables: {
      updateUserInput: {
        Firstname: stateValues.firstname,
        Lastname: stateValues.lastname,
        Email: stateValues.email,
        ContactNo: stateValues.contactno,
      },
      id:  user && user.id,
    },
    //refetchQueries: [{ query: GET_ALL_USERS }],
    //refetchQueries: [{ query: GET_USER_ID, variables: { id: props && props.userId }  }],
    //awaitRefetchQueries: true,
    update: (cache) => {
      cache.evict({
        id: 'ROOT_QUERY',
        field: 'id',
      })
    },
    onError: () => {
      //Do nothing
    },
  })
  const textHandler = (e) => {
    const { name, value } = e.target
    setStateValues({ ...stateValues, [name]: value })
    setStateValuesError({ ...stateValuesError, [name]: '' })
  }
  

  const saveHandler = async () => {
    let pErr = false

    // let pErr = false;
    // if (props.formType === 1) {
    //   if (stateValues.username === '') {
    //     setStateValuesError((prevState) => ({
    //       ...prevState,
    //       username: 'Please enter user name',
    //     }))
    //     pErr = true
    //   }
    // }

    if (!emailRegex.test(String(stateValues.email).toLowerCase())) {
      setStateValuesError((prevState) => ({
        ...prevState,
        email: 'Please enter valid email',
      }))
      pErr = true
    }
    if(!pErr){
      await updateUserMutation()
    }

  }


  useEffect(() => {
      if (dataUser) {
        console.log('data of single user', dataUser)
        setStateValues((prevState) => ({
          ...prevState,
          firstname: dataUser.UsersById.Firstname,
        }))
        setStateValues((prevState) => ({
          ...prevState,
          lastname: dataUser.UsersById.Lastname,
        }))
        setStateValues((prevState) => ({
          ...prevState,
          email: dataUser.UsersById.Email,
        }))
        setStateValues((prevState) => ({
          ...prevState,
          contactno: dataUser.UsersById.ContactNo,
        }))
      }
    
  }, [dataUser])


  return (
    <>
      <div
        className={classes.pageContent}
        style={{ padding: '80px 40px 80px 40px' }}
      >
        <div className={classes.root}>
          <Grid container>
            <Grid item xs={10} style={{ paddingRight: '15px' }}>
              <Card
                className={classes.root}
                variant='outlined'
                style={{ height: '100%' }}
              >
                <Grid container className={globalClasses.formContainer}>
                  <Grid container xs={12} className={globalClasses.gridItem}>

                    <Grid item xs={2}>
                      <Typography variant='h5'>First Name</Typography>
                    </Grid>

                    <Grid item xs={4}>
                      <FormControl fullWidth className={globalClasses.textField}>
                        <TextField
                          name='firstname'
                          variant='outlined'
                          placeholder='First Name'
                          onChange={textHandler}
                          //size="small"
                          value={stateValues.firstname}
                          inputProps={{
                            maxLength: 500,
                          }}
                        />
                      </FormControl>
                    </Grid>

                    <Grid item xs={2}>
                      <Typography variant='h5' style={{ marginLeft: '50px' }}>
                        Last Name
                      </Typography>
                    </Grid>

                    <Grid item xs={4}>
                      <FormControl fullWidth className={globalClasses.textField}>
                        <TextField
                          name='lastname'
                          variant='outlined'
                          placeholder='Last Name'
                          onChange={textHandler}
                          value={stateValues.lastname}
                          inputProps={{
                            maxLength: 500,
                          }}
                        />
                      </FormControl>
                    </Grid>
                  </Grid>



                  <Grid container xs={12} className={globalClasses.gridItem}>
                   
                    <Grid item xs={2}>
                      <Typography variant='h5'>
                        User Name<span className={globalClasses.asterikClass}>*</span>
                      </Typography>
                    </Grid>

                    <Grid item xs={4}>
                      <FormControl fullWidth className={globalClasses.textField}>
                        <TextField
                          name='username'
                          variant='outlined'
                          placeholder='User Name'
                          onChange={textHandler}
                          value={stateValues.username}
                          inputProps={{
                            maxLength: 500,
                          }}
                        />
                      </FormControl>
                      <Typography
                        className={classes.errorStyle}
                        component='p'
                        color='error'
                        variant='body2'
                      >
                       
                      </Typography>
                    </Grid>

                    <Grid item xs={2}>
                      <Typography variant='h5' style={{ marginLeft: '50px' }}>
                        Email<span className={classes.asterikClass}>*</span>
                      </Typography>
                    </Grid>

                    <Grid item xs={4}>
                      <FormControl fullWidth className={globalClasses.textField}>
                        <TextField
                          name='email'
                          variant='outlined'
                          placeholder='example@example.com'
                          onChange={textHandler}
                          value={stateValues.email}
                          inputProps={{
                            maxLength: 500,
                            readOnly: true,
                          }}
                        />
                      </FormControl>
                      <Typography
                        className={classes.errorStyle}
                        component='p'
                        color='error'
                        variant='body2'
                      >
                        {/* {stateValuesError.email} */}
                      </Typography>
                    </Grid>
                  </Grid>

                  <Grid container xs={12} className={globalClasses.gridItem}>
                    <Grid item xs={2}>
                      <Typography variant='h5'>User Group</Typography>
                    </Grid>

                    <Grid item xs={4}>
                      <FormControl fullWidth className={globalClasses.textField}>
                        <TextField
                          name='addgroup'
                          variant='outlined'
                          placeholder='User Group'
                          onChange={textHandler}
                          value={stateValues.addgroup}
                          inputProps={{
                            maxLength: 500,
                          }}
                        />
                      </FormControl>
                    </Grid>

                    <Grid item xs={2}>
                      <Typography variant='h5' style={{ marginLeft: '50px' }}>
                        Contact No
                      </Typography>
                    </Grid>

                    <Grid item xs={4}>
                      <FormControl fullWidth className={globalClasses.textField}>
                        <TextField
                          name='contactno'
                          variant='outlined'
                          placeholder='12345678'
                          onChange={textHandler}
                          value={stateValues.contactno}
                          inputProps={{
                            maxLength: 500,
                          }}
                        />
                      </FormControl>

                    </Grid>
                  </Grid>
                </Grid>
                <Grid className={globalClasses.modalFooter}>
                <Button autoFocus
                  onClick={() => props.history.push('/landing')}
                  className={globalClasses.cancelButton}
                >
                  Cancel
                </Button>
                <Button className={globalClasses.nextButton}
                onClick={saveHandler}
                >
                  Save
                </Button>
                </Grid>            
              </Card>
            </Grid>

            <Grid item xs={2} style={{ paddingLeft: '15px' }}>
              <Card className={classes.root} variant='outlined'>
                <CardContent>
                  <img
                    src='data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxIQEhAQDxASEA8QEhAQDxAPDw8VEBAQFREWFxUSFhYYHSggGBolGxUVITEhJSkrLy4uFx8zODMtNyg5LysBCgoKDQ0NDg8NGisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAOEA4QMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAABAUBAwYCB//EADgQAAIBAQUECAQFBQEBAAAAAAABAgMEBREhMRJBUXEGMlJhgZGhsSJywdEzQmKi8BMjssLhkoL/xAAUAQEAAAAAAAAAAAAAAAAAAAAA/8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAwDAQACEQMRAD8A+zgAAAAAAAAAAAAAAAAGu0VlTjKcnhGKbYGwFbZ79s88lU2XwmnH1eXqWMXjms09GtAMgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAN4ZvQobx6SwhjGiv6ku0+ouXaK7pJezqSlRg8KcXhLD88lr4IowOnum9U1Ovaa2aezTprdkm2oLXVLHnmVl9XzK0fDFONJPFR3yfGX2KsACTYrwqUXjTm0t8XnF80RgB3Fz3xG0LDq1EsXDiuMeKLM+b0K0oSjODwlF4pn0Gw2lVacKi/MscOD0a88QN4AAAAAAAAAAAAAAAAAAAAAAAAAAFDfPSBU26dHCU1lKX5YPguLNnSW83SgqcHhUqJ5rWMN75vTzONAyAAABjEDIMYmVnpnyAEqxXhVov+3Npb4vOL8CZdt2SzqVFsqKcoxeraWTa3IqEB3Fz3zG0fC1sVUs445SXGP2LQ+bU6ji1KLwlF4prVM726bcq9OM9JdWa4SWvhv8AECYAAAAAAAAAAAAAAAAAAAAAAGu0T2YTl2YyfkmwOEvi0/1a1SW7acY/LHJffxIZhGUgPVGlKbUYpuT0SL2yXFFZ1XtPsxeEV46smXXYVRjn15dd/wCq7iaBHp2GlHSnDxim/Nm1U4rSKXJI9gDzsrgvIykZAHmccU1xTXocSdwcheVHYqzjuxbXJ5r3AjF/0QtOFSdN6TjtL5o/8b8igJ9wz2bRRfGWz/6TX1A7wAAAAAAAAAAAAAAAAAAAAANNtWNOouNOa/azcGscuOQHzMn3LR26scdI4zfhp6tEOrT2XKL1i3F+DwLnozDOpLujFerfsgL0AAAAAAAAp+kNk2kqqWccpfLufg/cuDDWOTzT1TA4gm3IsbRR+dPyz+hi9bIqVRxXVa2o9ye70JPRintWiD7KnL9rXu0B2wAAAAAAAAAAAAAAAAAAAAAG8Mwa7S/hf83gcPfsMK9RpYKT214rP1xLLo2v7c3xnh5RX3NHSWlnTnxTi/DNe7JPRz8J/O/ZAWoAAAAAAAAAA57pL16fyv3JPQ+K26kn2VBPm8X7IjdJOvD5P9mWPR6ls04vfKTl4Y4L2A6MAAAAAAAAAAAAAAAAAAAAANddfC+RsDQHO33R2qMuMcJLw19GzxcEMKSfalJrlp9CyqQ1i81mmuKPMYpJJLBLJJaJAegAAAAAAAAABRdIKLlOlh+ZbC57X/S8s1JR2ILRbMVyWRiUU8MUng8VitHxRIskcZY8MwJoAAAAAAAAAAAAAAAAAAAAAAANFoobWa13kSUcG1wLIg2qOEueYGoAAAAAAAAAAeqdNyyXqTqNPZWG/eabFHJvwJIAAAAAAAAAAAAAAAAAAAAAAAAA02uGKx3r2NwArAe68Nl4eK5HgAAAAAABA32OCeL4PDxAlUoYJL+YnoAAAAAAAAAAAAAAAAAAAAAAAAAAAUN63xrCi8tJTW/uj9wJ1ue0/ha2o5a6PgyPSqp5aPemRbn6j+Z+yJNehtZrKXuBuBBVolHJ589TYrZ+n1AlAiO2cI+bNNStKWry4ICRXtOGUc3x3ImXNNbMlj8W1i1jng0synI9ao4zUotxklk1zA7AFbdV6qr8M8I1PSXLv7iyAAAAAAAAAAAAAAAAAAAAAabRa4U+vNR7sc/LUDcCnr3/AAXUg5d8sl9yvrX1Wlo1BfpWfm8QJd+XlrSpvuqSX+K+pRGTAFxc/UfzP2RPKGx2p03xi9V9V3lzQrxmsYvHit65oD1UpqWq+5FnZWtM/cmgCslBrVNeB5LUxh3AVaIttWEljll9WWtqtkaeWsuyt3PgUtWo5Nyk8WwPMXhmsms01qmdRdF4/wBWOEvxI6/qXaRyx7o1ZQalF4SWjQHbg52hf8114xmuK+F/YsbPfVKWrcH+tZeayAsQYhJNYpprinijIAAAAAAAAAA0Wy2QpLam+SXWlyA3lfbL4p08k9uXCOniykt96Tq4rqw7KevN7yABYWq+Ks8k9iPCGvnqQGzAAAAAAAB6hNxeMXg1vR5AFvY7xUsIzye6W58+BYHMFzddp2lsvWOnfECbJ4ZvJLVlTa7ybxVPJdre+XAXracXsLRdbvfArwMmAAAAAAADZRryg8YScX3P+YlrZb+ksqkVJdqOUvLR+hTADsrLbadXqSxe+LykvAkHDJ4ZrJrRrVFvYL8lHCNX449r8y+4HRA8Ua0ZpSg1KL3o9gAABot1qVKDm+SXGW5HI2mvKpJym8W/JLgu4tek1X4oQ3KLl4t4fQpQAAAAAAAAAAAAAAb7FV2Jp7s0+WBoAGZPHFvV5vmYAAAAAAAAAAAAAAAJNhtsqMsY6PrRekl9+86yzWiNSKnHR+ae9M4ouOjddqcqe6S2l8y/57AdEAAOZ6Rfi/8AxH3ZVlv0kj/ci+MF6SZUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACdcrwrU+bX7WQSdcqxrU+bf7WB1gAAoOk+tPlL3RSAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACxuD8aPKf+LAA6kAAf/Z'
                    style={{ width: '100%' }}
                  />
                </CardContent>
              </Card>
            </Grid>
          </Grid>
        </div>
      </div>
    </>
  )
}
export default UpdateProfile
